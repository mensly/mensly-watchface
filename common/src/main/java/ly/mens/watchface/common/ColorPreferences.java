package ly.mens.watchface.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.Uri;
import android.preference.PreferenceManager;

import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.PutDataRequest;

/**
 * Created by mensly on 7/08/2014.
 */
public class ColorPreferences {
    public static final String DATA_PATH = "/colors";
    public static final Uri DATA_URI = new Uri.Builder()
            .scheme(PutDataRequest.WEAR_URI_SCHEME)
            .path(DATA_PATH)
            .build();

    private static final String BOOL_DEFAULT = "use_defaults";
    private static final String COLOR_TIME = "colour.time";
    private static final String COLOR_DATE = "colour.date";
    private static final String COLOR_BG = "colour.bg";
    private static final String COLOR_SECONDS = "colour.sec";
    private static final String SUFFIX_ACTIVE = ".active";
    private static final String SUFFIX_INACTIVE = ".inactive";
    private static final boolean ALLOW_CONFIRM_DEFAULTS = false;

    private static final class ColorPair {
        public final boolean isDefault;
        public final int active;
        public final int inactive;

        public ColorPair(int colour) {
            this(colour, colour, false);
        }

        public ColorPair(int active, int inactive) {
            this(active, inactive, false);
        }

        public ColorPair(int active, int inactive, boolean isDefault) {
            this.active = active;
            this.inactive = inactive;
            this.isDefault = isDefault;
        }

        public int get(boolean active) {
            return active ? this.active : this.inactive;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ColorPair colorPair = (ColorPair) o;

            if (ALLOW_CONFIRM_DEFAULTS) {
                if (isDefault && colorPair.isDefault) return true;
                if (isDefault || colorPair.isDefault) return false;
            }

            if (active != colorPair.active) return false;
            if (inactive != colorPair.inactive) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = active;
            result = 31 * result + inactive;
            return result;
        }
    }

    private static int NOT_SET = Integer.MIN_VALUE;
    private ColorPair time;
    private ColorPair date;
    private ColorPair background;
    private ColorPair seconds;
    private boolean loaded = false;
    private boolean useDefaults = true;

    public void load(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        useDefaults = prefs.getBoolean(BOOL_DEFAULT, true);
        Resources res = context.getResources();
        time = loadFromApp(prefs, res, COLOR_TIME, R.color.time_normal, R.color.time_dimmed);
        date = loadFromApp(prefs, res, COLOR_DATE, R.color.date_normal, R.color.date_dimmed);
        background = loadFromApp(prefs, res, COLOR_BG, R.color.background, R.color.background);
        seconds = loadFromApp(prefs, res, COLOR_SECONDS, R.color.seconds, R.color.seconds);
        loaded = true;
    }

    private static ColorPair loadFromApp(SharedPreferences prefs, Resources res, String key, int defaultActive, int defaultInactive) {
        int colour = prefs.getInt(key, NOT_SET);
        if (colour == NOT_SET) {
            int active = prefs.getInt(key + SUFFIX_ACTIVE, NOT_SET);
            int inactive = prefs.getInt(key + SUFFIX_INACTIVE, NOT_SET);
            if (active != NOT_SET && inactive != NOT_SET) {
                return new ColorPair(active, inactive);
            }
            else {
                return new ColorPair(res.getColor(defaultActive), res.getColor(defaultInactive), true);
            }
        }
        else {
            return new ColorPair(colour);
        }
    }

    public void load(Context context, DataMap map) {
        if (!loaded) {
            load(context);
        }
        if (map.getBoolean(BOOL_DEFAULT, true)) {
            restoreDefaults(context);
        }
        else {
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
            this.time = loadFromData(map, COLOR_TIME, editor, this.time);
            this.date = loadFromData(map, COLOR_DATE, editor, this.date);
            this.background = loadFromData(map, COLOR_BG, editor, this.background);
            this.seconds = loadFromData(map, COLOR_SECONDS, editor, this.seconds);
            editor.apply();
        }
    }

    private ColorPair loadFromData(DataMap map, String key, SharedPreferences.Editor editor, ColorPair current) {
        ColorPair newColour;
        int colour = map.getInt(key);
        if (colour == NOT_SET) {
            int active = map.getInt(key + SUFFIX_ACTIVE, NOT_SET);
            int inactive = map.getInt(key + SUFFIX_INACTIVE, NOT_SET);
            if (active != NOT_SET && inactive != NOT_SET) {
                newColour = new ColorPair(active, inactive);
            }
            else {
                newColour = current;
            }
        }
        else {
            newColour = new ColorPair(colour);
        }
        if (!newColour.equals(current)) {
            save(editor, key, newColour);
        }
        return newColour;
    }

    private SharedPreferences.Editor save(SharedPreferences.Editor editor, String key, ColorPair colour) {
        if (colour.active == colour.inactive) {
            editor.remove(key + SUFFIX_ACTIVE);
            editor.remove(key + SUFFIX_INACTIVE);
            editor.putInt(key, colour.active);
        }
        else {
            editor.remove(key);
            editor.putInt(key + SUFFIX_ACTIVE, colour.active);
            editor.putInt(key + SUFFIX_INACTIVE, colour.inactive);
        }
        useDefaults = false;
        editor.putBoolean(BOOL_DEFAULT, false);
        return editor;
    }

    public void save(DataMap map) {
        save(map, COLOR_TIME, time);
        save(map, COLOR_DATE, date);
        save(map, COLOR_BG, background);
        save(map, COLOR_SECONDS, seconds);
        map.putBoolean(BOOL_DEFAULT, useDefaults);
    }

    private void save(DataMap map, String key, ColorPair colour) {
        if (colour.active == colour.inactive) {
            map.putInt(key, colour.active);
        }
        else {
            map.putInt(key + SUFFIX_ACTIVE, colour.active);
            map.putInt(key + SUFFIX_INACTIVE, colour.inactive);
        }
    }

    public boolean isLoaded() {
        return loaded;
    }

    public int getTime(boolean active) {
        return time.get(active);
    }

    public void setTime(Context context, int active, int inactive) {
        setTime(context, new ColorPair(active, inactive));
    }

    private void setTime(Context context, ColorPair time) {
        if (!time.equals(this.time)) {
            this.time = time;
            save(PreferenceManager.getDefaultSharedPreferences(context).edit(), COLOR_TIME, time)
                    .apply();
        }
    }

    public int getDate(boolean active) {
        return date.get(active);
    }

    public void setDate(Context context, int active, int inactive) {
        setDate(context, new ColorPair(active, inactive));
    }

    private void setDate(Context context, ColorPair date) {
        if (!date.equals(this.date)) {
            this.date = date;
            save(PreferenceManager.getDefaultSharedPreferences(context).edit(), COLOR_DATE, date)
                    .apply();
        }
    }

    public int getBackground(boolean active) {
        return background.get(active);
    }

    public void setBackground(Context context, int active, int inactive) {
        setBackground(context, new ColorPair(active, inactive));
    }

    private void setBackground(Context context, ColorPair background) {
        if (!background.equals(this.background)) {
            this.background = background;
            save(PreferenceManager.getDefaultSharedPreferences(context).edit(), COLOR_BG, background)
                    .apply();
        }
    }

    public int getSeconds(boolean active) {
        return seconds.get(active);
    }

    public void setSeconds(Context context, int active, int inactive) {
        setSeconds(context, new ColorPair(active, inactive));
    }

    private void setSeconds(Context context, ColorPair seconds) {
        if (!seconds.equals(this.seconds)) {
            this.seconds = seconds;
            save(PreferenceManager.getDefaultSharedPreferences(context).edit(), COLOR_SECONDS, seconds)
                    .apply();
        }
    }

    public void restoreDefaults(Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit()
                .clear()
                .apply();
        load(context);
    }
}

Mensly Face
===================

This is a watch face that works the way I like it. Day/date display, 12 hour time and my favourite
 colours. I also put a neat effect for the seconds. Feel free to grab it and modify to suit your
 own digital watch requirements, or let me know any simple ideas you think would make it better. I
 have made it entirely to suit my own desires, but it is pretty easy to work with and modify.

This project is based on the [Watch Template by Two Toasters][1], which is a great place to start
 if you're interested in making watches for Android Wear. My modifications based on that template,
 (essentially just [Watchface.java][2], [SecondsCircleView.java][3]  and the contents
 of [/res/][4]) are licensed under the [WTFPL][5].


[1]: https://github.com/twotoasters/watchface-template
[2]: wear/src/main/java/ly/mens/watchface/widget/Watchface.java
[3]: wear/src/main/java/ly/mens/watchface/widget/SecondsCircleView.java
[4]: wear/src/main/res/
[5]: copying.txt

package ly.mens.watchface;

import android.app.Application;

import timber.log.Timber;

/**
 * Created by mensly on 8/08/2014.
 */
public class CompanionApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
    }
}

package ly.mens.watchface;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.Wearable;

import butterknife.ButterKnife;
import butterknife.InjectView;
import hugo.weaving.DebugLog;
import ly.mens.watchface.common.ColorPreferences;

/**
 * Created by mensly on 7/08/2014.
 */
public class ConfigActivity extends FragmentActivity implements SeekBar.OnSeekBarChangeListener,
        View.OnClickListener, ColorFragment.Callbacks, GoogleApiClient.ConnectionCallbacks {

    @InjectView(R.id.time_inactive) SeekBar timeInactive;
    @InjectView(R.id.time_inactive_preview) ImageView timeInactivePreview;
    @InjectView(R.id.time_active) TextView timeActive;
    @InjectView(R.id.date_inactive) SeekBar dateInactive;
    @InjectView(R.id.date_inactive_preview) ImageView dateInactivePreview;
    @InjectView(R.id.date_active) TextView dateActive;
    @InjectView(R.id.bg_inactive) SeekBar bgInactive;
    @InjectView(R.id.bg_inactive_preview) ImageView bgInactivePreview;
    @InjectView(R.id.bg_active) TextView bgActive;
    @InjectView(R.id.seconds_active) TextView secondsActive;

    private ColorPreferences colours = new ColorPreferences();
    private final float tmpHsv[] = new float[3];
    private GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        if (BuildConfig.DEBUG) { findViewById(android.R.id.content).setKeepScreenOn(true); }
        ButterKnife.inject(this);
        timeInactive.setOnSeekBarChangeListener(this);
        dateInactive.setOnSeekBarChangeListener(this);
        bgInactive.setOnSeekBarChangeListener(this);
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(Wearable.API)
                .build();
    }

    @Override
    protected void onResume() {
        super.onResume();
        colours.load(this);
        refreshDisplay();
        googleApiClient.connect();
    }

    private void refreshDisplay() {
        timeInactive.setProgress(getGrey(colours.getTime(false)));
        timeActive.setBackgroundColor(colours.getTime(true));
        timeActive.setTextColor(getContrastColor(colours.getTime(true)));

        dateInactive.setProgress(getGrey(colours.getDate(false)));
        dateActive.setBackgroundColor(colours.getDate(true));
        dateActive.setTextColor(getContrastColor(colours.getDate(true)));

        bgInactive.setProgress(getGrey(colours.getBackground(false)));
        bgActive.setBackgroundColor(colours.getBackground(true));
        bgActive.setTextColor(getContrastColor(colours.getBackground(true)));

        secondsActive.setBackgroundColor(colours.getSeconds(true));
        secondsActive.setTextColor(getContrastColor(colours.getSeconds(true)));
    }

    @Override
    protected void onPause() {
        super.onPause();
        googleApiClient.disconnect();
    }

    private int getGrey(int colour) {
        Color.colorToHSV(colour, tmpHsv);
        return (int)(255 * tmpHsv[2]);
    }

    private int getContrastColor(int colour) {
        Color.colorToHSV(colour, tmpHsv);
        return tmpHsv[1] < 0.3f && tmpHsv[2] > 0.5f ? Color.BLACK : Color.WHITE;
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        int gray = (255 * seekBar.getProgress() / seekBar.getMax());
        int colour = Color.rgb(gray, gray, gray);
        ImageView target;
        switch (seekBar.getId()) {
            case R.id.time_inactive:
                target = timeInactivePreview;
                if (fromUser) {
                    colours.setTime(this, colours.getTime(true), colour);
                }
                break;
            case R.id.date_inactive:
                target = dateInactivePreview;
                if (fromUser) {
                    colours.setDate(this, colours.getDate(true), colour);
                }
                break;
            case R.id.bg_inactive:
                target = bgInactivePreview;
                if (fromUser) {
                    colours.setBackground(this, colours.getBackground(true), colour);
                }
                break;
            default:
                return; // Skip
        }
        target.setImageAlpha(gray);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    @DebugLog
    public void onStopTrackingTouch(SeekBar seekBar) {
        syncData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.time_active:
            case R.id.date_active:
            case R.id.bg_active:
            case R.id.seconds_active:
                int colour = Color.BLACK;
                Drawable background = v.getBackground();
                if (background instanceof ColorDrawable) {
                    colour = ((ColorDrawable) background).getColor();
                }
                ColorFragment.createInstance(v.getId(), colour).show(getSupportFragmentManager(), null);
                break;
            case R.id.btn_defaults:
                colours.restoreDefaults(this);
                refreshDisplay();
                syncData();
                break;
        }
    }

    @Override
    public void onColorSelected(ColorFragment fragment, int srcId, int color) {
        switch (srcId) {
            case R.id.time_active:
                colours.setTime(this, color, colours.getTime(false));
                break;
            case R.id.date_active:
                colours.setDate(this, color, colours.getDate(false));
                break;
            case R.id.bg_active:
                colours.setBackground(this, color, colours.getBackground(false));
                break;
            case R.id.seconds_active:
                colours.setSeconds(this, color, colours.getSeconds(false));
                break;
        }
        View target = findViewById(srcId);
        if (target != null) {
            target.setBackgroundColor(color);
            if (target instanceof TextView) {
                ((TextView) target).setTextColor(getContrastColor(color));
            }
        }
        syncData();
    }

    @Override
    public void onConnected(Bundle bundle) {
        syncData();
    }

    @Override
    public void onConnectionSuspended(int id) {

    }

    @DebugLog
    private void syncData() {
        if (googleApiClient.isConnected()) {
            PutDataMapRequest request = PutDataMapRequest.create(ColorPreferences.DATA_PATH);
            DataMap map = request.getDataMap();
            colours.save(map);
            Wearable.DataApi.putDataItem(googleApiClient, request.asPutDataRequest());
        }
    }

}

package ly.mens.watchface;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.SaturationBar;
import com.larswerkman.holocolorpicker.ValueBar;

import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by mensly on 7/08/2014.
 */
public class ColorFragment extends DialogFragment implements View.OnClickListener, ColorPicker.OnColorChangedListener {

    public static interface Callbacks {
        void onColorSelected(ColorFragment fragment, int srcId, int color);
    }
    private static final Callbacks dummyCallbacks = new Callbacks() {
        @Override
        public void onColorSelected(ColorFragment fragment, int srcId, int color) {
        }
    };

    @InjectView(R.id.picker) ColorPicker picker;
    @InjectView(R.id.saturation_bar) SaturationBar saturationBar;
    @InjectView(R.id.value_bar) ValueBar valueBar;
    @InjectView(R.id.hex_value) EditText hexDisplay;
    @InjectView(R.id.btn_confirm) Button confirm;

    public static final String INT_SRC_ID = "src";
    public static final String INT_COLOR = "colour";
    private int srcId;
    private int oldColour;
    private int colour;
    private Callbacks callbacks = dummyCallbacks;

    public static ColorFragment createInstance(int srcId, int color) {
        Bundle args = new Bundle();
        args.putInt(INT_COLOR, color);
        args.putInt(INT_SRC_ID, srcId);
        ColorFragment f = new ColorFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            colour = args.getInt(INT_COLOR, Color.WHITE);
            srcId = args.getInt(INT_SRC_ID);
        }
        oldColour = colour;
        if (savedInstanceState != null) {
            colour = savedInstanceState.getInt(INT_COLOR, colour);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (outState != null) {
            outState.putInt(INT_COLOR, colour);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof Callbacks) {
            callbacks = (Callbacks)activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = dummyCallbacks;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Dialog d = getDialog();
        if (d != null) {
            d.setTitle(R.string.fragment_color_title);
        }
        return inflater.inflate(R.layout.fragment_colour, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(this, view);
        picker.addSaturationBar(saturationBar);
        picker.addValueBar(valueBar);
        picker.setOldCenterColor(oldColour);
        picker.setColor(colour);
        picker.setOnColorChangedListener(this);
        confirm.setOnClickListener(this);
        onColorChanged(colour);
    }

    @Override
    public void onColorChanged(int colour) {
        this.colour = colour;
        hexDisplay.setText(String.format(Locale.ENGLISH, "#%06X", colour ^ Color.BLACK));
    }

    @Override
    public void onClick(View v) {
        confirm();
    }

    private void confirm() {
        callbacks.onColorSelected(this, srcId, colour);
        dismiss();
    }
}

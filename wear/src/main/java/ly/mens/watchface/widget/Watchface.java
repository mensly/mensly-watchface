package ly.mens.watchface.widget;

import android.content.Context;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.Wearable;
import com.twotoasters.watchface.gears.widget.IWatchface;
import com.twotoasters.watchface.gears.widget.Watch;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;
import hugo.weaving.DebugLog;
import ly.mens.watchface.R;
import ly.mens.watchface.common.ColorPreferences;
import timber.log.Timber;


public class Watchface extends FrameLayout implements IWatchface, GoogleApiClient.ConnectionCallbacks, DataApi.DataListener {

    @InjectView(R.id.hours) TextView hoursView;
    @InjectView(R.id.separator) TextView separatorView;
    @InjectView(R.id.minutes) TextView minutesView;
    @InjectView(R.id.ampm) TextView amPmView;
    @InjectView(R.id.date) TextView dateView;
    @InjectView(R.id.seconds) SecondsCircleView secondsView;

    private static final boolean ENABLE_CONFIG = false;
    private static final SimpleDateFormat FORMAT_HOURS = new SimpleDateFormat("h");
    private static final SimpleDateFormat FORMAT_HOURS_24 = new SimpleDateFormat("k");
    private static final SimpleDateFormat FORMAT_MINUTES = new SimpleDateFormat("mm");
    private static final SimpleDateFormat FORMAT_AMPM = new SimpleDateFormat("a");
    private static final SimpleDateFormat FORMAT_DATE = new SimpleDateFormat("EEEE, d MMMM yyyy");

    private Watch watch;

    private boolean inflated;
    private boolean active;
    private int lastMinute = Integer.MIN_VALUE;
    private boolean showAmPm = true;
    private ColorPreferences colours = new ColorPreferences();
    private GoogleApiClient googleApiClient;


    public Watchface(Context context) {
        super(context);
        init(context, null, 0);
    }

    public Watchface(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public Watchface(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    @DebugLog
    private void init(Context context, AttributeSet attrs, int defStyle) {
        if (isInEditMode()) { return; }
        watch = new Watch(this);
        watch.setTickInterval(1000 / 12);
        loadSettings(context);

        if (ENABLE_CONFIG) {
            googleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(this)
                    .addApi(Wearable.API)
                    .build();
        }
    }

    @DebugLog
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.inject(this, getRootView());
        inflated = true;
    }

    @DebugLog
    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (isInEditMode()) { return; }
        watch.onAttachedToWindow();
    }

    @DebugLog
    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (isInEditMode()) { return; }
        watch.onDetachedFromWindow();
    }

    @Override
    public void onTimeChanged(Calendar time) {
        int minute = time.get(Calendar.MINUTE);
        if (minute != lastMinute) {
            Date now = time.getTime();
            this.hoursView.setText((showAmPm ? FORMAT_HOURS : FORMAT_HOURS_24).format(now));
            this.minutesView.setText(FORMAT_MINUTES.format(now));
            this.amPmView.setText(FORMAT_AMPM.format(now).toUpperCase(Locale.ENGLISH));
            this.dateView.setText(FORMAT_DATE.format(now));
            lastMinute = minute;
        }

        if (active) {
            secondsView.setSecondsPhase(time.get(Calendar.SECOND) / 60f + time.get(Calendar.MILLISECOND) / 60000f);
        }

    }

    @Override
    @DebugLog
    public void onActiveStateChanged(boolean active) {
        this.active = active;
        refreshViewState();
        if (ENABLE_CONFIG) {
            if (active) {
                Wearable.DataApi.addListener(googleApiClient, this);
                googleApiClient.connect();
            } else {
                Wearable.DataApi.removeListener(googleApiClient, this);
                googleApiClient.disconnect();
            }
        }
    }

    @Override
    public boolean handleSecondsInDimMode() {
        return false;
    }

    @DebugLog
    private void refreshViewState() {
        if (inflated) {
            int timeColour = colours.getTime(active);
            hoursView.setTextColor(timeColour);
            separatorView.setTextColor(timeColour);
            minutesView.setTextColor(timeColour);
            amPmView.setTextColor(timeColour);
            amPmView.setVisibility(showAmPm ? View.VISIBLE : View.GONE);
            dateView.setTextColor(colours.getDate(active));
            secondsView.setVisibility(active ? View.VISIBLE : View.GONE);
            secondsView.setColor(colours.getSeconds(active));
            setBackgroundColor(colours.getBackground(active));
        }
    }

    private void loadSettings(Context context) {
        colours.load(context);
        showAmPm = !DateFormat.is24HourFormat(context);
        refreshViewState();
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        if (ENABLE_CONFIG) {
            for (DataEvent event : dataEvents) {
                if (event.getType() == DataEvent.TYPE_CHANGED) {
                    postDataMap(DataMap.fromByteArray(event.getDataItem().getData()));
                }
            }
        }
    }

    private void postDataMap(final DataMap map) {
        if (ENABLE_CONFIG) {
            Timber.d(map.toString());
            post(new Runnable() {
                @Override
                public void run() {
                    colours.load(getContext(), map);
                    refreshViewState();
                    googleApiClient.disconnect();
                }
            });
        }
    }
}

package ly.mens.watchface.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import ly.mens.watchface.R;

/**
 * Created by mensly on 5/08/2014.
 */
public class SecondsCircleView extends View {
    private static final int CIRCLES_PER_SECOND = 5;
    private Paint paint;
    private float secondsPhase;
    private int alpha;

    public SecondsCircleView(Context context) {
        super(context);
        init(context);
    }

    public SecondsCircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SecondsCircleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        final Resources res = context.getResources();
        paint.setColor(res.getColor(R.color.seconds));
        paint.setStrokeWidth(res.getDimension(R.dimen.seconds));
        secondsPhase = isInEditMode() ? 0.5f : Float.NaN;
    }

    public void setSecondsPhase(float secondsPhase) {
        this.secondsPhase = secondsPhase;
        invalidate();
    }

    public void setColor(int colour) {
        paint.setColor(colour);
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (visibility != VISIBLE) {
            this.secondsPhase = Float.NaN;
        }
    }

    @Override
    public void setAlpha(float alpha) {
        super.setAlpha(alpha);
        this.alpha = (int)(255 * alpha);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (!Float.isNaN(secondsPhase)) {
            // Draw seconds
            paint.setAlpha(255);
            final float halfHeight = canvas.getHeight() / 2;
            final float halfWidth = canvas.getWidth() / 2;
            final float maxRadius = Math.min(halfWidth, halfHeight);

            float phase = this.secondsPhase * CIRCLES_PER_SECOND;
            final int keyScale = (int)phase;
            phase -= keyScale;
            for (int scale = 0; scale < CIRCLES_PER_SECOND; scale++) {
                float effectivePhase = (scale + phase) / CIRCLES_PER_SECOND;
                float alpha = 1 - (float)Math.pow(effectivePhase, scale == keyScale ? 20 : 2);
                if (alpha > 0) {
                    if (scale != keyScale) {
                        alpha /= 2;
                    }
                    paint.setAlpha((int) (this.alpha * alpha));
                    canvas.drawCircle(halfWidth, halfHeight, effectivePhase * maxRadius, paint);
                }
            }
        }
    }
}
